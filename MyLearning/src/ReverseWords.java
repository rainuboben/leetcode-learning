public class ReverseWords {
    public static String reverseWords(String s) {
        // Split the string into words using one or more spaces as delimiter
        String[] words = s.trim().split("\\s+");

        // Reverse the order of words
        StringBuilder reversed = new StringBuilder();
        for (int i = words.length - 1; i >= 0; i--) {
            reversed.append(words[i]).append(" ");
        }

        // Convert StringBuilder to String and remove the trailing space
        return reversed.toString().trim();
    }

    public static void main(String[] args) {
        String s1 = "the sky is blue";
        System.out.println(reverseWords(s1)); // Output: "blue is sky the"

        String s2 = "  hello world  ";
        System.out.println(reverseWords(s2)); // Output: "world hello"

        String s3 = "a good   example";
        System.out.println(reverseWords(s3)); // Output: "example good a"
    }
}
