package MergeTwoString;

public class MergeTwoString {
    public String mergeAlternately(String word1, String word2) {
        String word3 = "";
        int word1Length = word1.length();
        int word2Length = word2.length();
        if (word1Length == 0)
            return word2;
        else if (word2Length == 0)
            return word1;
        else {
            int minLength = Math.min(word1Length, word2Length);
            int i = 0;
            while (i < minLength) {
                word3 = word3.concat(word1.substring(i, i+1));
                word3 = word3.concat(word2.substring(i, i+1));
                i++;
            }
            if (word1Length == minLength && word2Length == minLength) {
                return word3;
            } else if (word1Length > minLength) {
                word3 = word3.concat(word1.substring(i, word1Length));
            } else if (word2Length > minLength) {
                word3 = word3.concat(word2.substring(i, word2Length));
            }
            return word3;
        }
    }
}
