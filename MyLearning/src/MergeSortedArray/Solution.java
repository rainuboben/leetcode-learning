package MergeSortedArray;

import java.util.Arrays;

public class Solution {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int totalElements = m + n - 1;
        int firstArray = m - 1;
        int secondArray = n - 1;
        while ( secondArray >= 0) {
            if (firstArray >= 0 && nums1[firstArray] > nums2[secondArray]) {
                nums1[totalElements] = nums1[firstArray];
                firstArray--;
            } else {
                nums1[totalElements] = nums2[secondArray];
                secondArray--;
            }
            totalElements--;
        }
//        if (firstArray < 0 && secondArray < 0) {
//            System.out.println("Sorted Array : " + Arrays.toString(nums1));
//        } else if (secondArray >= 0) {
//            while (secondArray >= 0) {
//                nums1[totalElements--] = nums2[secondArray--];
////                totalElements--;
//            }
//        }
        System.out.println("Sorted Array : " + Arrays.toString(nums1));
    }

}


